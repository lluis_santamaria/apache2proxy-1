Apache2 Proxy
==========================

Useful when you need to run more than one container on the same web port

Build the docker image:

```sh
$ docker build -t apache2_proxy .
```

Build the docker container:

```
$ docker run -dit -p 80:80 -p 443:443 --name apache2_proxy apache2_proxy
```

If you have some ssl files:
```
$ docker cp ssl/. apache2_proxy:/home/ssl
```

Go into the container:

```
$ docker exec -it apache2_proxy bash
$ a2enmod ssl
$ service apache2 restart
```