FROM debian:wheezy

RUN apt-get update
RUN apt-get install -y nano htop

RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install \
        curl \
        apache2 && \
    	rm -rf /var/lib/apt/lists/*

COPY ./proxy.conf /etc/apache2/sites-available/proxy.conf

RUN mkdir /home/ssl
RUN a2dissite default
RUN a2ensite proxy.conf
RUN a2enmod proxy && a2enmod proxy_http && service apache2 reload && service apache2 restart

EXPOSE 80