#!/bin/bash

CONTAINER_NAME=apache2_proxy
IMAGE_NAME=apache2_proxy_image

docker build -t apache2_proxy_image .

CONTAINER_RUNNING_ID="$(docker ps -q -f name=$CONTAINER_NAME)"

#Check if container is running
if [ ! "$CONTAINER_RUNNING_ID" ]; then
	CONTAINER_STOPPED_ID="$(docker ps -aq -f status=exited -f name=$CONTAINER_NAME)"
	if [ "$CONTAINER_STOPPED_ID" ]; then
        	echo "Container $CONTAINER_NAME already exists. Running image...$IMAGE_NAME"
        	docker start $CONTAINER_NAME
    	else
		docker run -dit -p 80:80 --name $CONTAINER_NAME $IMAGE_NAME
	fi

#Container already running
else
	echo "Existing container with ID=$CONTAINER_RUNNING_ID"
	while [[ $CONTAINER_RUNNING_ID != "" ]]
    	do
		echo "Stopping running container with ID=$CONTAINER_RUNNING_ID"
		docker stop $CONTAINER_RUNNING_ID
		sleep 3
		CONTAINER_RUNNING_ID="$(docker ps -q -f name=$CONTAINER_NAME)"
	done
	echo "Starting container with image $IMAGE_NAME"
	docker start $CONTAINER_NAME
fi

docker exec -ti $CONTAINER_NAME sh -c "service apache2 reload && service apache2 restart"
